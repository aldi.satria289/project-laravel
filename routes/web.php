<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



//User
Route::get('/user', 'UserController@index');
Route::get('/user/{id}', 'UserController@show');
Route::get('/user/{id}/edit', 'UserController@edit');
Route::put('/user/{id}', 'UserController@update');
Route::delete('/user/{id}', 'UserController@delete');

//Post
Route::get('/post/create', 'PostController@create');
Route::post('/home', 'PostController@store');
Route::get('/home', 'PostController@index')->name('posts.index');
Route::get('/post/{id}', 'PostController@show');
Route::get('/post/{id}/edit', 'PostController@edit');
Route::put('/post/{id}', 'PostController@update');
Route::delete('/post/{id}', 'PostController@destroy');

//Auth
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

//follow
Route::get('/follow/{id}', 'FollowController@store');
Route::get('/follow/{id_show}/{id_follow}/delete', 'FollowController@destroy');

//komentar
Route::post('/comment', 'CommentController@store');
Route::get('/comment/{id}/edit', 'CommentController@edit');
Route::delete('/comment/{id}', 'CommentController@destroy');
Route::put('/comment/{id}', 'CommentController@update');



//Like post
Route::get('/post/like/{id}', [
    'uses' => 'PostController@like',
    'as'   => 'post.like'
]);
Route::get('/post/unlike/{id}', [
    'uses' => 'PostController@unlike',
    'as'   => 'post.unlike'
]);

//Like comment
Route::get('/comment/like/{id}', [
    'uses' => 'CommentController@like',
    'as'   => 'comment.like'
]);

Route::get('/comment/unlike/{id}', [
    'uses' => 'CommentController@unlike',
    'as'   => 'comment.unlike'
]);

//Post bergambar
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});