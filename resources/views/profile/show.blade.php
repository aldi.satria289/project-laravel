@extends('adminlte.master')

@section('content')

<section class="content">
    <div class="card mx-auto mb-5" style="width: 80%;">
        <div class="card-header bg-info text-white">
          <h3 class="card-title">Profile</h3> 
        </div>
        
        <div class="card-body text-center">
            <div class="mt-3 mb-2 ">
                <h4 class="display-4 te">{{$user->name}}</h4>
                <p class="lead">{{$user->address}}</p>
                @if ($user->id == Auth::id())
                <a class="btn btn-warning btn-sm" href="/user/{{$user->id}}/edit">Edit Profile</a>
                @else
                    @php
                    $showFollow = "<a class='btn btn-warning btn-sm' href='/follow/".$user->id."'>Follow</a>";
                    foreach ($user->follower as $follower) {
                        // echo $follower->pivot;
                        if (Auth::id() == $follower->pivot->following) {
                            $showFollow = "<a class='btn btn-warning btn-sm' href='/follow/".$user->id."/".$follower->pivot->id."/delete'>Unfollow</a>";
                        }
                    }
                    echo $showFollow;
                    @endphp
                @endif
            </div>
            <div class="box-footer mb-3">
                <div class="row">
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                            <h5 class="description-header">{{count($user->posts)}}</h5> <span class="description-text">POSTS</span>
                        </div>
                    </div>
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                            <h5 class="description-header">{{count($user->follower)}}</h5> <span class="description-text">FOLLOWERS</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="description-block">
                            <h5 class="description-header">{{count($user->following)}}</h5> <span class="description-text">FOLLOWING</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    @foreach ($user->posts as $post)
    <div class="card mx-auto mb-3" style="width: 80%;">
        <div class="card-header" style="border-left:3px solid rgb(240, 228, 67);">
            <a href="/user/{{Auth::id()}}" class="card-title" style="font-weight: 600; color: #333;"><i class="fas fa-portrait"></i> {{$user->name}}</a>
            
        </div> 
        <div class="card-body">
            <h5> {!!$post->post!!} </h5>
            
        </div>
            <!-- /.card-body -->
        <div class="card-footer " style="display: flex; background-color: #f0f0f0; border-bottom: 2px solid #aaa;">
           
              @if($post->is_liked_by_auth_user())
            <a href="{{ route ('post.unlike', ['id' => $post->id])}}" class="btn btn-danger mr-1"><i class="fas fa-thumbs-down"></i><span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->likes->count()}} Likes</span></a>
            @else
            <a href="{{ route ('post.like', ['id' => $post->id])}}" class="btn btn-info mr-1"><i class="fas fa-thumbs-up"></i><span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->likes->count()}} Likes</span></a>
            @endif
            
            <a href="/post/{{$post->id}}" class="btn btn-info mr-1"><i class="fas fa-comments"></i><span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->likes->count()}} Komentar</span></a>
              <div class="ml-auto"><h6> {{$post->updated_at->diffForHumans() }} </h6></div>
        </div>
          
    </div>
    @endforeach
            
</section>
@endsection