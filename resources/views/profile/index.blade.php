@extends('adminlte.master')

@section('content')
<div class="card mx-auto" style="width: 80%;">
      <div class="card-header bg-info">
        <h3 class="card-title">Find Friends</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Alamat</th>
            <th style="width:10%">Show</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($users as $key => $user)
            @if ($user->id == Auth::id())
                
            @else
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->address}}</td>
                <td> <a class="btn btn-info btn-sm mx-1" href="/user/{{$user->id}}">Show</a></td>
            </tr>
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>


@endsection

@push('script-table')
    <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
@endpush