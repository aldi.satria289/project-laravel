@extends('adminlte.master')

@section('content')
{{-- <form action="/user/{{$user->id}}" method="post">
  @csrf
  @method('put')
  Nama : <input type="text" name="name" id="name" value="{{old('name', $user->name)}}"> <br>
  Email : <input type="text" name="email" id="email" value="{{old('email', $user->email)}}"> <br>
  Photo : <input type="text" name="photo" id="photo" value="{{old('photo', $user->photo)}}"> <br>
  Gender : 
  <select name="gender" id="gender">
      <option value="male">male</option>
      <option value="female">female</option>
  </select>
  <br>
  Address : <input type="text" name="address" id="address" value="{{old('address', $user->address)}}"> <br>
  <button type="submit">update</button>
</form> --}}


<div class="card card-primary mx-auto mb-5" style="width: 50%;">
  <div class="card-header">
    <h3 class="card-title">Edit Profile</h3>
  </div>
  <!-- form start -->
  <form role="form" action="/user/{{$user->id}}" method='post'>
    @csrf
    @method('put')
    <div class="card-body">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name='name' value="{{old('name', $user->name)}}" placeholder="Masukkan Nama">
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name='email' value="{{old('email', $user->email)}}" placeholder="Masukkan Email">
      </div>
      <div class="form-group">
      <label for="gender">Gender</label>
        <div class="form-check">
          <input class="form-check-input" value="male" type="radio" name="gender" {{ (old('gender', $user->gender) == 'male') ? 'checked' : ''}}>
          <label class="form-check-label">Male</label>
        </div>
        <div class="form-check">
          <input class="form-check-input" value="female" type="radio" name="gender" {{ (old('gender', $user->gender) == 'female') ? 'checked' : ''}}>
          <label class="form-check-label">Female</label>
        </div>
      </div>
      <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" id="address" name='address' value="{{old('address', $user->address)}}" placeholder="Masukkan Address">
      </div>
      {{-- <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name='password' value="" placeholder="Masukkan Password">
      </div>
      <div class="form-group">
        <label for="password_confirm">Confirm Password</label>
        <input type="password" class="form-control" id="password_confirm" name='password_confirm' value="" placeholder="Masukkan Confirm Password">
      </div>
    </div> --}}

    <div class="card-footer">
        <a href='/user/{{$user->id}}' class="btn btn-warning">Kembali</a>
      <button type="submit" class="btn btn-primary">Update</button>
    </div>
  </form>
</div>

@endsection