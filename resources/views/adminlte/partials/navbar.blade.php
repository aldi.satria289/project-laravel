<nav class="main-header navbar navbar-expand navbar-white navbar-light fixed-top" style="background-color:#fafafa; margin-left: 0px;">
    <!-- Left navbar links -->
    <ul class="navbar-nav d-sm-inline-flex" style="margin-left:8%">
      <a href="/home"> <img id="logo" src="{{ asset('/adminlte/dist/img/logo.png')}}" width="250px;" style=" margin-left:10%; margin-right:50px;"></a>
      <li class="nav-item d-none d-sm-inline-flex" style="margin: 10px; text-align:center;">
        <a href="/home" class="nav-link"><i class="fas fa-home"></i>Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-flex" style="margin: 10px; text-align:center;">
        <a href="/user" class="nav-link"><i class="fas fa-user-friends"></i>Find Friends</a>
      </li>
      <li class="nav-item d-none d-sm-inline-flex" style="margin: 10px; text-align:center;">
        <a href="/user/{{Auth::id()}}" class="nav-link"><i class="fas fa-portrait"></i>Profile</a>
      </li>
      

    </ul>

    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto mr-5">
    @guest
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
              @if (Route::has('register'))
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                  </li>
              @endif
          @else
              <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
              </li>
       @endguest
    </ul>
  </nav>