@extends('adminlte.master')

@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Danu Falka</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action='/pertanyaan' method='POST'>
                <div class="card-body">
                  <div class="form-group">
                        <label>Create Post</label>
                        <textarea class="form-control" id="isi" name="isi" rows="3"  placeholder="Enter ..."></textarea>
                     
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <a class="btn btn-info btn-sm" href="/buku">Balik</a>
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
@endsection


