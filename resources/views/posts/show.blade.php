@extends('adminlte.master')

@section('content')
<div class="card mx-auto" style="width: 80%;">
  <div class="card-header" style="border-left:3px solid rgb(240, 228, 67);">
    <a href="/user/{{$post->user->id}}" class="card-title" style="font-weight: 600; color: #333;"><i class="fas fa-portrait"></i> {{$post->user->name}}</a>
  </div> 
  
  <div class="card-body">
  <h5> {!!$post->post!!} </h5>
  
  </div>
  <!-- /.card-body -->
  <div class="card-footer " style="display: flex; background-color: #f0f0f0; border-bottom: 2px solid #aaa;">
 
  @if($post->is_liked_by_auth_user())
    <a href="{{ route ('post.unlike', ['id' => $post->id])}}" class="btn btn-danger mr-1">
      <i class="fas fa-thumbs-down"></i>
      <span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->likes->count()}} Likes</span>
    </a>
  @else
    <a href="{{ route ('post.like', ['id' => $post->id])}}" class="btn btn-info mr-1">
      <i class="fas fa-thumbs-up"></i>
      <span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->likes->count()}} Likes</span>
    </a>
  @endif
  
  <a href="/post/{{$post->id}}" class="btn btn-info mr-1"><i class="fas fa-comments"></i><span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->comments->count()}} Komentar</span></a>
    <div class="ml-auto d-inline-flex p-2">
        @if (Auth::id() == $post->user->id)
          <a class="mr-1 btn btn-primary btn-sm" href="/post/{{$post->id}}/edit"><i class="fas fa-edit"></i></a>
          <form action="/post/{{$post->id}}" method="POST">
            @csrf
            @method('DELETE')  
            <button type="submit" class="mr-2 btn btn-danger btn-sm">
              <i class="fas fa-trash-alt"></i></button>
          </form>
        @endif

      <h6> {{$post->updated_at->diffForHumans() }} </h6>
      
    </div>
  </div>
</div>


<div class="card mx-auto" style="width: 60%; padding:10px; background-color: #fafafa; border-bottom: 3px solid #999;">
<div class="card-body">
<table class="table table-striped">
<thead>
  {{-- comment --}} 
  <form role="form" action="/comment" method="POST">
    @csrf
    <tr>
    <div class="form-group">
      <input type="hidden" name="post_id" value="{{$post->id}}">   
      <th style="width: 10%"><label  for="komentar">Komentar </label></th>
      <th style="width: 70%"><input type="text" class="form-control" name="komentar" id="komentar"></th>
      <th style="width: 20%"><button type="submit" class="btn btn-success">Send</button></th>
      </div>
    </tr>
  </form>
  {{-- end comment --}}
  </thead>
  <tbody>
  @foreach ($post->comments as $key => $comment)
    <tr>
      <td>
        <p class="font-weight-bold">{{$comment->name}}</p>
      </td>
      <td>
        <h5>{{$comment->pivot->comment}}</h5> <br> 
        <p class="font-italic">{{$comment->updated_at->diffForHumans() }} </p>
      </td>
      <td>
      {{-- show edit delete button --}}
      <div class="d-inline-flex p-3 ">
        @if ($comment->id == Auth::id())
          <a class="mr-1 btn btn-primary btn-sm" href="/comment/{{$comment->pivot->id}}/edit"><i class="fas fa-edit"></i></a>
          <form action="/comment/{{$comment->pivot->id}}" method="POST">
            @csrf
            @method('DELETE')  
            <input type="hidden" value="{{$post->id}}" name="post_id">
            <button type="submit" class="btn mr-1 btn-danger btn-sm">
              <i class="fas fa-trash-alt"></i>
            </button>
          </form>
        @endif

        @if ($comments[$key]->is_liked())
          <a href="{{ route ('comment.unlike', ['id' => $comments[$key]->id])}}" class="mr-1 btn btn-danger btn-sm">
            <i class="fas fa-thumbs-down"></i>
          </a>{{ $comments[$key]->likes->count()}}
        @else
          <a href="{{ route ('comment.like', ['id' => $comments[$key]->id])}}" class="mr-1 btn btn-primary btn-sm">
            <i class="fas fa-thumbs-up"></i>
          </a>{{ $comments[$key]->likes->count()}}
        @endif
        </div>
      </td>
    </tr>
    @endforeach
  </tbody>
  </table>
  </div>
</div>

@endsection