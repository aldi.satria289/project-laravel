@extends('adminlte.master')

@section('content')
<section class="content">
  
      <div class="card mx-auto" style="width: 50%;">
          <a class="pt-2 pb-2 btn btn-primary" href="/post/create"><i class="fas fa-plus"></i> Buat Postingan Baru</a>
      </div>
      
      
        @if(session('success'))
          <div class="card mx-auto" style="width: 80%;">
              <div class="alert alert-success">
              {{session('success')}}
            </div>
          </div>
         @endif
      @forelse ($posts as $key =>$post )
      <div class="card mx-auto mb-3" style="width: 80%;">
        <div class="card-header" style="border-left:3px solid rgb(240, 228, 67);">
          <a href="/user/{{$post->user->id}}" class="card-title" style="font-weight: 600; color: #333;"><i class="fas fa-portrait"></i> {{$post->user->name}}</a>
        </div> 
        
        <div class="card-body">
        <h5> {!!$post->post!!} </h5>
        
        </div>
        <!-- /.card-body -->
        <div class="card-footer " style="display: flex; background-color: #f0f0f0; border-bottom: 2px solid #aaa;">
       
        @if($post->is_liked_by_auth_user())
          <a href="{{ route ('post.unlike', ['id' => $post->id])}}" class="btn btn-danger mr-1">
            <i class="fas fa-thumbs-down"></i>
            <span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->likes->count()}} Likes</span>
          </a>
        @else
          <a href="{{ route ('post.like', ['id' => $post->id])}}" class="btn btn-info mr-1">
            <i class="fas fa-thumbs-up"></i>
            <span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->likes->count()}} Likes</span>
          </a>
        @endif
        
        <a href="/post/{{$post->id}}" class="btn btn-info mr-1"><i class="fas fa-comments"></i><span class="badge" style="font-size: .875rem; padding: .5em .4em;">{{ $post->comments->count()}} Komentar</span></a>
          <div class="ml-auto"><h6> {{$post->updated_at->diffForHumans() }} </h6></div>
        </div>
        </div>
        @empty
        <div class="card mx-auto mb-3" style="width: 80%;">
        <h2 class="mt-3 mb-3 card-title" align="center">Tidak Ada Postingan</h2>
        </div>
        @endforelse
        <!-- /.card-footer-->
      
                    
</section>

@endsection