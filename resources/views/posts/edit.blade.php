@extends('adminlte.master')

@section('content')
  <div class="ml-3 mt-3 mr-3 mx-auto" style="width: 80%;">
   <div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-secondary"><i class="fas fa-edit"></i> Edit Post #{{$post->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/post/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Postingan Anda</label>
          <textarea class="form-control" id="post" name="post" placeholder="Enter ...">{!!old('post', $post->post)!!}</textarea>
          @error('post')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-secondary"><i class="fas fa-check"></i> Submit</button>
          </div></div>
        </form>
    </div>
   </div>

@endsection


@push('script-textgambar')
{{-- File Manager --}}
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<script>
 var route_prefix = "/filemanager";
</script>
<!-- TinyMCE init -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "",
    selector: "textarea[name=post]",
    plugins: [
      "link image"
    ],
    relative_urls: false,
    height: 129,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
  
</script>
@endpush