@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="text-align: center;">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <br><br>
                    <a class="btn btn-success btn-sm mr-1" href="/home"><i class="fas fa-home"></i> Go To Homepage</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
