@extends('adminlte.master')

@section('content')
<div class="card mx-auto" style="width: 60%; padding:10px; background-color: #fafafa; border-bottom: 3px solid #999;">
<div class="card-body">
<table class="table table-striped">
<thead>
  {{-- comment --}} 
  <form role="form" action="/comment/{{$comment->id}}" method="POST">
    @csrf
    @method('PUT')
    <tr>
    <div class="form-group">
      <input type="hidden" name="post_id">   
      <th style="width: 10%"><label  for="komentar">Komentar </label></th>
      <th style="width: 70%"><input type="text" class="form-control" name="komentar" id="komentar" value="{{old('komentar', $comment->comment)}}"></th>
      <th style="width: 20%"><button type="submit" class="btn btn-success">Send</button></th>
      </div>
    </tr>
  </form>
  {{-- end comment --}}
  </thead>
  </table>
  </div>
</div>

@endsection