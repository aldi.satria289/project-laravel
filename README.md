<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Membuat Sosial Media

Deskripsi singkat: 
Aplikasi web untuk mengupdate status, profil, gambar, tulisan atau cerita. 

Terdapat Beberapa fitur utama:
1. User harus terdaftar di web untuk menggunakan layanan sosmed
2. User dapat membuat postingan berupa: tulisan, gambar dengan caption, quote. 
3. User dapat membuat, mengedit, dan menghapus pada postingan milik sendiri. 
4. Seorang User dapat mengikuti (follow) ke banyak User lainnya. Seorang User dapat diikuti oleh banyak User lainnya. 
5. Satu postingan dapat disukai(like) oleh banyak User. Satu User dapat menyukai (like) banyak Postingan.
6. Satu postingan dapat memiliki banyak komentar. Satu komentar dimiliki oleh satu postingan.
7. Satu komentar dapat disukai oleh banyak User. Satu User dapat menyukai banyak komentar. 
8. Seorang User dapat membuat dan mengubah profile nya sendiri. 
9. Pada halaman menampilkan postingan terdapat konten posting, komentar, jumlah komentar, jumlah like.
10. Pada halaman profile User terdapat biodata, jumlah user pengikut(follower), jumlah user yang diikuti (following)


## Package
**Laravel File Manager**

**TL;DR**
Run these lines

 ```
composer require unisharp/laravel-filemanager
 php artisan vendor:publish --tag=lfm_config
 php artisan vendor:publish --tag=lfm_public
 php artisan storage:link
```

Edit APP_URL in .env.

**Full Installation Guide**
Install package

```
 composer require unisharp/laravel-filemanager
(optional) Edit config/app.php :
```


* For Laravel 5.5 and up, skip to step 3. All service providers and facades are automatically discovered.

Add service providers

```
 UniSharp\LaravelFilemanager\LaravelFilemanagerServiceProvider::class,
 Intervention\Image\ImageServiceProvider::class,
And add class aliases
```


 'Image' => Intervention\Image\Facades\Image::class,
Code above is for Laravel 5.1. In Laravel 5.0 should leave only quoted class names.

Publish the package’s config and assets :

```
 php artisan vendor:publish --tag=lfm_config
 php artisan vendor:publish --tag=lfm_public
```

(optional) Run commands to clear cache :

```
 php artisan route:clear
 php artisan config:clear
```

Ensure that the files & images directories (in config/lfm.php) are writable by your web server (run commands like chown or chmod).

Create symbolic link :

` php artisan storage:link`
Edit APP_URL in .env.

Edit routes/web.php :

Create route group to wrap package routes.

```
 Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });
```


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
