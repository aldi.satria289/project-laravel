<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'gender', 'photo', 'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function likes()
    {
        return $this->belongsToMany('App\Post', 'likes', 'user_id', 'post_id');
    }

    public function comments()
    {
        return $this->belongsToMany('App\Post', 'comments', 'user_id', 'post_id');
    }

    public function following()
    {
        return $this->belongsToMany('App\User', 'follows', 'following', 'followed')->withPivot('id');
    }

    public function follower()
    {
        return $this->belongsToMany('App\User', 'follows', 'followed', 'following')->withPivot('id');
    }
    
    public function likes_comment()
    {
        return $this->belongsToMany('App\Comment', 'like_comment', 'user_id', 'comment_id')->withPivot('id');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
