<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Post extends Model
{
    protected $table = "posts";

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function comments() {
        return $this->belongsToMany('App\User', 'comments', 'post_id', 'user_id')->withPivot('comment', 'id');
    }

    public function likes() {
        return $this->hasMany('App\Like');
    }

    public function is_liked_by_auth_user() {
        $id = Auth::id();
        $likers = array();
        foreach ($this->likes as $like):
            array_push($likers, $like->user_id);
        endforeach;

        if(in_array($id, $likers)) {
            return true;
        }else {
            return false;
        }

    }

}
