<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\LikeComment;
use Auth;
use Session;
use DB;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'komentar' => 'required',
        ]);

        $comment = Comment::create([  // Mass Assignment
            "comment" => $request["komentar"],
            "user_id" => Auth::user()->id,
            "post_id" => $request["post_id"],
        ]); 
        return redirect('/post/'.$request["post_id"])->with('success', 'Postingan Berhasil Dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::find($id);
        // echo $comment;
        return view('comment.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post_id = Comment::find($id)->post_id;
        $update = Comment::where('id', $id)->update([
            'comment' => $request['komentar'],
        ]);
        return redirect('/post/'.$post_id)->with('success', 'Komentar Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // echo $request;
        // echo $id;
        $comment = Comment::find($id);

        $comment->likes()->delete();
        $comment->delete();
        
        return redirect('/post/'.$request['post_id'])->with('success', 'Komentar Berhasil Dihapus!');
    }

    public function like($id){    
        LikeComment::create([
            'comment_id' => $id,
            'user_id' => Auth::id()
        ]);

        Session::flash('success','You liked the post');
        return redirect()->back();
    }

    public function unlike($id){
        $like = LikeComment::where('comment_id', $id)->where('user_id', Auth::id())->first();
        $like->delete();

        Session::flash('success','You unliked the post');
        return redirect()->back();
    }
}
