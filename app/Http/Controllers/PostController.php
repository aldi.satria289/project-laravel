<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use App\Like;
use App\Post;
use App\Comment;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all(); // Eloquent
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'post' => 'required'
        ]);
        $posts = Post::create([  // Mass Assignment
            "post" => $request["post"],
            "user_id" => Auth::user()->id,
        ]); 
        return redirect('/home')->with('success', 'Postingan Berhasil Dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $comments = Comment::where("post_id", "=", $id)->get();
        // echo $post;
        // echo $comment;

        // return view('posts.show', compact('post'));
        return view('posts.show', ['post' => $post, 'comments' => $comments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Post::where('id', $id)->update([
            'post' => $request['post'],
        ]);
        return redirect('/home')->with('success', 'Postingan Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return redirect('/home')->with('success', 'Post Berhasil Dihapus!');
    }
    public function like($id){
        
        Like::create([
            'post_id' => $id,
            'user_id' => Auth::id()
        ]);

        Session::flash('success','You liked the post');
        return redirect()->back();
    }
    public function unlike($id){
        $like = Like::where('post_id', $id)->where('user_id', Auth::id())->first();
        $like->delete();

        Session::flash('success','You unliked the post');
        return redirect()->back();
    }
}
