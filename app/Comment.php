<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Comment extends Model
{
    protected $guarded = [];

    // public function likes_comment()
    // {
    //     return $this->belongsToMany('App\User', 'like_comment', 'comment_id', 'user_id')->withPivot('id');
    // }

    public function likes() {
        return $this->hasMany('App\LikeComment');
    }

    public function is_liked() {
        $id = Auth::id();
        $likers = array();
        foreach ($this->likes as $like):
            array_push($likers, $like->user_id);
        endforeach;

        if(in_array($id, $likers)) {
            return true;
        }else {
            return false;
        }

    }
}
